package com.asos.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Stelian Morariu on 8/8/2015.
 */
public class CategoryListing {

    @SerializedName("Description")
    private String description;

    @SerializedName("SortType")
    private String sortType;

    @SerializedName("Listing")
    private List<Category> listing;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public List<Category> getListing() {
        return listing;
    }

    public void setListing(List<Category> listing) {
        this.listing = listing;
    }
}
