package com.asos.android.model;

import android.support.annotation.NonNull;

/**
 * Helper class designed to handle favorited and shopping cart items.
 */
public class ProcesedProduct {

    private long mProductId;
    private int mQuantity;

    /**
     * Default empty constructor.
     */
    public ProcesedProduct() {

    }

    /**
     * Create a new {@link ProcesedProduct} using the given product id.
     *
     * @param productId the id of the product
     */
    public ProcesedProduct(long productId) {
        mProductId = productId;
    }

    public ProcesedProduct(long productId, int quantity) {
        mProductId = productId;
        mQuantity = quantity;
    }

    public long getProductId() {
        return mProductId;
    }

    public void setProductId(@NonNull long productId) {
        this.mProductId = productId;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(@NonNull int quantity) {
        this.mQuantity = quantity;
    }


}
