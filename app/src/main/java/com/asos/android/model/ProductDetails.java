package com.asos.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Stelian Morariu on 14/8/2015.
 */
public class ProductDetails {
    @SerializedName("ProductId")
    private long productId;

    @SerializedName("Title")
    private String title;

    @SerializedName("Brand")
    private String brand;

    @SerializedName("BasePrice")
    private double basePrice;

    @SerializedName("PriceType")
    private String priceType;

    @SerializedName("Colour")
    private String color;

    @SerializedName("InStock")
    private boolean isInStock;

    @SerializedName("IsInSet")
    private boolean isInSet;

    @SerializedName("CurrentPrice")
    private String currentPrice;

    @SerializedName("PreviousPrice")
    private String previousPrice;

    @SerializedName("ProductImageUrls")
    private List<String> productImages;

    @SerializedName("RRP")
    private String recomendedRetailPrice;

    @SerializedName("Size")
    private String size;

    @SerializedName("Sku")
    private String sku;

    @SerializedName("AdditionalInfo")
    private String aditionalInfo;

    @SerializedName("AssociatedProducts")
    private List<ProductDetailCompact> associatedProducts;

    @SerializedName("CareInfo")
    private String careInfo;

    @SerializedName("Description")
    private String description;

    @SerializedName("Variants")
    private List<ProductDetailCompact> variants;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isInStock() {
        return isInStock;
    }

    public void setIsInStock(boolean isInStock) {
        this.isInStock = isInStock;
    }

    public boolean isInSet() {
        return isInSet;
    }

    public void setIsInSet(boolean isInSet) {
        this.isInSet = isInSet;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(String previousPrice) {
        this.previousPrice = previousPrice;
    }

    public List<String> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<String> productImages) {
        this.productImages = productImages;
    }

    public String getRecomendedRetailPrice() {
        return recomendedRetailPrice;
    }

    public void setRecomendedRetailPrice(String recomendedRetailPrice) {
        this.recomendedRetailPrice = recomendedRetailPrice;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getAditionalInfo() {
        return aditionalInfo;
    }

    public void setAditionalInfo(String aditionalInfo) {
        this.aditionalInfo = aditionalInfo;
    }

    public List<ProductDetailCompact> getAssociatedProducts() {
        return associatedProducts;
    }

    public void setAssociatedProducts(List<ProductDetailCompact> associatedProducts) {
        this.associatedProducts = associatedProducts;
    }

    public String getCareInfo() {
        return careInfo;
    }

    public void setCareInfo(String careInfo) {
        this.careInfo = careInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProductDetailCompact> getVariants() {
        return variants;
    }

    public void setVariants(List<ProductDetailCompact> variants) {
        this.variants = variants;
    }
}
