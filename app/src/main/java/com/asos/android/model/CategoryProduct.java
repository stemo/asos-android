package com.asos.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Stelian Morariu on 8/8/2015.
 */
public class CategoryProduct {

    @SerializedName("ProductId")
    private long productId;

    @SerializedName("Title")
    private String title;

    @SerializedName("Brand")
    private String brand;

    @SerializedName("BasePrice")
    private double basePrice;

    @SerializedName("CurrentPrice")
    private String currentPrice;

    @SerializedName("HasMoreColours")
    private boolean hasMoreColours;

    @SerializedName("IsInSet")
    private boolean isInSet;

    @SerializedName("PreviousPrice")
    private String previousPrice;

    @SerializedName("ProductImageUrl")
    private List<String> productImages;

    @SerializedName("RRP")
    private String recomendedRetailPrice;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    public boolean isHasMoreColours() {
        return hasMoreColours;
    }

    public void setHasMoreColours(boolean hasMoreColours) {
        this.hasMoreColours = hasMoreColours;
    }

    public boolean isInSet() {
        return isInSet;
    }

    public void setIsInSet(boolean isInSet) {
        this.isInSet = isInSet;
    }

    public String getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(String previousPrice) {
        this.previousPrice = previousPrice;
    }

    public List<String> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<String> productImages) {
        this.productImages = productImages;
    }

    public String getRecomendedRetailPrice() {
        return recomendedRetailPrice;
    }

    public void setRecomendedRetailPrice(String recomendedRetailPrice) {
        this.recomendedRetailPrice = recomendedRetailPrice;
    }
}
