package com.asos.android;

import android.app.Application;

import com.asos.android.model.CategoryListing;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by Stelian Morariu on 8/8/2015.
 */
public class AsosApp extends Application {

    private CategoryListing mMensCategory;
    private CategoryListing mWomensCategory;

    @Override
    public void onCreate() {
        super.onCreate();

        // configure Picasso to use OkHttpDownloader in order to obtain automatic caching
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(false);
        Picasso.setSingletonInstance(built);
    }

    public CategoryListing getMensCategory() {
        return mMensCategory;
    }

    public void setMensCategory(CategoryListing mensCategory) {
        this.mMensCategory = mensCategory;
    }

    public CategoryListing getWomensCategory() {
        return mWomensCategory;
    }

    public void setWomensCategory(CategoryListing womensCategory) {
        this.mWomensCategory = womensCategory;
    }
}
