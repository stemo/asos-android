package com.asos.android;

/**
 * Created by Muerte on 8/8/2015.
 */
public class Constants {
    public static final String PRODUCTS_API_URL = "https://dl.dropboxusercontent.com/u/1559445/ASOS/SampleApi/";
    public static final String WOMEN_CATEGORY_URL = "/cats_women.json";
    public static final String MEN_CATEGORY_URL = "/cats_men.json";
    public static final String SAMPLE_CATEGORY_PRODUCTS_URL = "/anycat_products.json";
    public static final String SAMPLE_PRODUCT_URL = "/anyproduct_details.json";
    public static final String PRODUCT_ID = "prodId";
    public static final String CATEGORY_ID = "catId";
    public static final String WISHLIST_KEY = "asos_wishlist";
    public static final String SHOPPING_CART_KEY = "asos_cart";
}
