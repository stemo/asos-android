package com.asos.android.utils;

import android.support.annotation.NonNull;

import com.asos.android.model.CategoryListing;

/**
 * Callback that notifies when a  {@link CategoryListing} has been downloaded.
 * <p/>
 * Created by Stelian Morariu on 8/8/2015.
 */
public interface CategoryListingCallback {

    void onCategoryListingDownloaded(@NonNull CategoryListing categoryListing,
                                     @NonNull CategoryType categoryType);

    void onError(String error);
}
