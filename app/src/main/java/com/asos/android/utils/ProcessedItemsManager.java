package com.asos.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.asos.android.model.ProcesedProduct;
import com.asos.android.model.CategoryProduct;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class designed to handle wishlist items.
 * <p/>
 * Created by Stelian Morariu on 13/8/2015.
 */
public abstract class ProcessedItemsManager {

    protected static Type mProcessedProductListType;
    protected static Gson mGson;
    protected static SharedPreferences mSharedPreferences;

    protected Context mContext;

    protected abstract String getPreferenceKey();
    protected abstract List<ProcesedProduct> getItemsList();

    /**
     * Add the product with the given id to the list of processed items.
     *
     * @param productId
     */
    public void add(@NonNull long productId) {
        getItemsList().add(new ProcesedProduct(productId));
        updatePreferences();
    }

    /**
     * Add the product with the given id and quantity to the list of processed items.
     *
     * @param productId
     * @param quantity
     */
    public void add(@NonNull long productId, @NonNull int quantity) {
        getItemsList().add(new ProcesedProduct(productId, quantity));
        updatePreferences();
    }

    /**
     * Return the quantity for a {@link ProcesedProduct}.
     *
     * @param productId
     * @return the quantity for the product with the given {@code productId}
     * or 0 if there is no product with the given {@code productId} or there is no quantity set
     */
    public int getProcessedProductQuantity(@NonNull long productId) {
        int quantity = 0;
        final ProcesedProduct procesedProduct = findInProcessedList(productId);

        if (procesedProduct != null) {
            try {
                quantity = procesedProduct.getQuantity();
            } catch (NullPointerException npe) {
                // this shouldn't happen ->
                // if the quantity is null we already return 0
            }
        }

        return quantity;
    }

    /**
     * Remove the given {@link CategoryProduct} from the processed items list (only if it is found).
     *
     * @param productId the id of the product to remove
     */
    public void remove(@NonNull long productId) {
        final ProcesedProduct processed = findInProcessedList(productId);
        if (processed != null) {
            getItemsList().remove(processed);
            updatePreferences();
        }
    }


    /**
     * Check if a Product with the given id is in the list of processed items.
     *
     * @param productId the id of the product to search for
     * @return whether the given Product is processed or not
     */
    public boolean isProcessed(@NonNull long productId) {
        return findInProcessedList(productId) != null;
    }

    /**
     * Save current wishlist to {@link SharedPreferences}.
     */
    private void updatePreferences() {
        final SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(getPreferenceKey(), mGson.toJson(getItemsList()));
        editor.commit();
    }

    private ProcesedProduct findInProcessedList(long productId) {
        ProcesedProduct wishlisted = null;

        for (ProcesedProduct pp : getItemsList()) {
            if (pp.getProductId() == productId) {
                wishlisted = pp;
                break;
            }
        }

        return wishlisted;
    }

    /**
     * Get a list of all the items.
     *
     * @return
     */
    public List<ProcesedProduct> getItems() {
        return new ArrayList<>(getItemsList());
    }
}
