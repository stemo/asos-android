package com.asos.android.utils;

/**
 * Created by Stelian Morariu on 8/8/2015.
 */
public enum CategoryType {
    WOMEN,
    MEN;
}
