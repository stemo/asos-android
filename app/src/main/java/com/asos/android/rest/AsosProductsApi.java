package com.asos.android.rest;

import com.asos.android.Constants;
import com.asos.android.model.CategoryListing;
import com.asos.android.model.CategoryProduct;
import com.asos.android.model.CategoryProductListWrapper;
import com.asos.android.model.ProductDetails;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * This is the API used to retrieve ASOS product information.
 * Created by Stelian Morariu on 8/8/2015.
 */
public interface AsosProductsApi {

    /**
     * Get subcategories available for {@link com.asos.android.utils.CategoryType.WOMEN}
     *
     * @return availabe subcategories
     */
    @GET(Constants.WOMEN_CATEGORY_URL)
    CategoryListing getWomenCategories();


    /**
     * Get subcategories available for {@link com.asos.android.utils.CategoryType.MEN}
     *
     * @return availabe subcategories
     */
    @GET(Constants.MEN_CATEGORY_URL)
    CategoryListing getMenCategories();


    /**
     * Get sample products for the given subcategory.
     *
     * @param categoryId the id of the subcategory
     * @return the products for the given subcategory
     */
    @GET(Constants.SAMPLE_CATEGORY_PRODUCTS_URL)
    CategoryProductListWrapper getSubCategoryProducts(@Query(Constants.CATEGORY_ID) String categoryId);

    /**
     * Get details about a product.
     *
     * @param productId the id of the product
     * @return a {@link CategoryProduct} for the given id
     */
    @GET(Constants.SAMPLE_PRODUCT_URL)
    ProductDetails getProduct(@Query(Constants.PRODUCT_ID) String productId);
}
