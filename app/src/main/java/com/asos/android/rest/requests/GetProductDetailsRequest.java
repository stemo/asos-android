package com.asos.android.rest.requests;

import android.support.annotation.NonNull;

import com.asos.android.model.CategoryProduct;
import com.asos.android.model.ProductDetails;
import com.asos.android.rest.AsosProductsApi;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * This request will return the details for the {@link CategoryProduct} with the given id.
 * <p/>
 * Created by Stelian Morariu on 8/8/2015.
 */
public class GetProductDetailsRequest extends RetrofitSpiceRequest<ProductDetails, AsosProductsApi> {

    private long mProductId;

    /**
     * Create a new {@link GetProductDetailsRequest}.
     *
     * @param productId the id of the product for which to get the details.
     */
    public GetProductDetailsRequest(@NonNull long productId) {
        super(ProductDetails.class, AsosProductsApi.class);
        mProductId = productId;
    }

    @Override
    public ProductDetails loadDataFromNetwork() throws Exception {
        return getService().getProduct(String.valueOf(mProductId));
    }
}
