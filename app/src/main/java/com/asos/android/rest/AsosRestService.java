package com.asos.android.rest;

import android.app.Application;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.asos.android.Constants;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.retrofit.GsonRetrofitObjectPersisterFactory;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

public class AsosRestService extends RetrofitGsonSpiceService {

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(AsosProductsApi.class);
    }

    @Override
    protected String getServerUrl() {
        return Constants.PRODUCTS_API_URL;
    }

    /**
     * Oveerriding this function will enable caching for requests.
     * <p/>
     * We will use  {@link com.google.gson.Gson} to persist cache.
     */
    @Override
    public CacheManager createCacheManager(Application application) {
        CacheManager cacheManager = new CacheManager();
        try {
            final GsonRetrofitObjectPersisterFactory gsonRetrofitObjectPersisterFactory = new GsonRetrofitObjectPersisterFactory(application);
            cacheManager.addPersister(gsonRetrofitObjectPersisterFactory);
        } catch (CacheCreationException e) {
            e.printStackTrace();
        }
        return cacheManager;
    }
}
