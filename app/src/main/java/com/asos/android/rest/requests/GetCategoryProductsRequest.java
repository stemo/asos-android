package com.asos.android.rest.requests;

import android.support.annotation.NonNull;

import com.asos.android.model.CategoryProductListWrapper;
import com.asos.android.rest.AsosProductsApi;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * This request will return the products available for the {@link com.asos.android.model.Category} with the given id.
 * <p/>
 * Created by Stelian Morariu on 8/8/2015.
 */
public class GetCategoryProductsRequest extends RetrofitSpiceRequest<CategoryProductListWrapper, AsosProductsApi> {

    private String mCatgegoryId;

    /**
     * Create a new {@link GetCategoryProductsRequest}.
     *
     * @param categoryId the id of the category for which to get the products.
     */
    public GetCategoryProductsRequest(@NonNull String categoryId) {
        super(CategoryProductListWrapper.class, AsosProductsApi.class);
        mCatgegoryId = categoryId;
    }

    @Override
    public CategoryProductListWrapper loadDataFromNetwork() throws Exception {
        return getService().getSubCategoryProducts(mCatgegoryId);
    }

    /**
     * Get a unique key to be used when caching requests.
     *
     * @param categoryId the id of the {@link com.asos.android.model.Category } for which the request is made
     * @return
     */
    public static String getCacheKey(String categoryId) {
        return GetCategoriesRequest.class.getSimpleName() + "_" + categoryId;
    }
}
