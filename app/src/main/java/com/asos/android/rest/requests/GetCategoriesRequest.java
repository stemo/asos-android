package com.asos.android.rest.requests;

import android.support.annotation.NonNull;

import com.asos.android.model.CategoryListing;
import com.asos.android.rest.AsosProductsApi;
import com.asos.android.utils.CategoryType;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * This request will return the subcategories available for the given {@link CategoryType}.
 * <p/>
 * Created by Stelian Morariu on 8/8/2015.
 */
public class GetCategoriesRequest extends RetrofitSpiceRequest<CategoryListing, AsosProductsApi> {

    private CategoryType mCatgegoryType;

    /**
     * Create a new {@link GetCategoriesRequest}.
     *
     * @param categoryType the {@link CategoryType} for which to return subcategories
     */
    public GetCategoriesRequest(@NonNull CategoryType categoryType) {
        super(CategoryListing.class, AsosProductsApi.class);
        mCatgegoryType = categoryType;
    }

    @Override
    public CategoryListing loadDataFromNetwork() throws Exception {
        switch (mCatgegoryType) {
            case MEN:
                return getService().getMenCategories();
            case WOMEN:
                return getService().getWomenCategories();
            default:
                return null;
        }
    }

    /**
     * Get a unique key to be used when caching requests.
     *
     * @param type the {@link CategoryType } for which the request is made
     * @return
     */
    public static String getCacheKey(String type) {
        return GetCategoriesRequest.class.getSimpleName() + "_" + type;
    }
}
