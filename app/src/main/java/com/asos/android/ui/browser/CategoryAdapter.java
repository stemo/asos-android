package com.asos.android.ui.browser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.asos.android.R;
import com.asos.android.model.Category;
import com.asos.android.utils.CategoryType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Stelian Morariu on 12/8/2015.
 */
public class CategoryAdapter extends ArrayAdapter<CategoryType> {

    private Context mContext;
    private List<CategoryType> mCategoryTypes;

    public CategoryAdapter(Context context) {
        super(context, R.layout.spinner_item_category, Arrays.asList(CategoryType.values()));
        mContext = context;
        mCategoryTypes = new ArrayList<>();
        mCategoryTypes.addAll(Arrays.asList(CategoryType.values()));
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, false);
    }


    public View getCustomView(int position, View convertView, ViewGroup parent, boolean isDropDown) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.spinner_item_category, parent, false);
        }
        final CategoryType category = mCategoryTypes.get(position);
        final TextView categoryName = (TextView) convertView.findViewById(R.id.name);
        categoryName.setText(category.toString());

        if (isDropDown) {
            categoryName.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
        } else {
            categoryName.setTextColor(mContext.getResources().getColor(R.color.accent));
        }

        return convertView;
    }
}
