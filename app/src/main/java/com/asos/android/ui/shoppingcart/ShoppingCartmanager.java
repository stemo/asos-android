package com.asos.android.ui.shoppingcart;

import android.content.Context;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.asos.android.Constants;
import com.asos.android.model.ProcesedProduct;
import com.asos.android.ui.wishlist.WishListManager;
import com.asos.android.utils.ProcessedItemsManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stelian Morariu on 13/8/2015.
 */
public class ShoppingCartManager extends ProcessedItemsManager {

    private static ShoppingCartManager mInstance;
    private static List<ProcesedProduct> mProcessedProducts;

    private ShoppingCartManager() {
    }

    private ShoppingCartManager(Context context) {
        mContext = context;
        mGson = new Gson();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mProcessedProductListType = new TypeToken<List<ProcesedProduct>>() {
        }.getType();
    }

    /**
     * Get an instance of {@link WishListManager}.
     *
     * @param context
     */
    public static ShoppingCartManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ShoppingCartManager(context);
            mProcessedProducts = new ArrayList<>();

            // load items from prefs
            final String json = mSharedPreferences.getString(Constants.SHOPPING_CART_KEY, "");
            if (!TextUtils.isEmpty(json)) {
                final ArrayList<ProcesedProduct> items = mGson.fromJson(json, mProcessedProductListType);
                mProcessedProducts.addAll(items);
            }

        }

        return mInstance;
    }

    @Override
    protected String getPreferenceKey() {
        return Constants.SHOPPING_CART_KEY;
    }

    @Override
    protected List<ProcesedProduct> getItemsList() {
        return mProcessedProducts;
    }
}
