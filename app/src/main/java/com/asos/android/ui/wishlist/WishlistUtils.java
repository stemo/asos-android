package com.asos.android.ui.wishlist;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.asos.android.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Helper class created in order to group common wishlist functionality.
 * Created by Stelian Morariu on 14/8/2015.
 */
public class WishlistUtils {

    /**
     * Update the given {@link ImageView} to display the icon corresponding to the favorite state and colorize it.
     *
     * @param context
     * @param target  the target {@link ImageView}
     */
    public static void setAsWished(@NonNull final Context context, @NonNull final ImageView target) {
        Picasso.with(context)
                .load(R.drawable.ic_action_favorite)
                .into(target, new Callback() {
                    @Override
                    public void onSuccess() {
                        target.getDrawable().
                                setColorFilter(context.getResources().getColor(R.color.accent),
                                        PorterDuff.Mode.SRC_ATOP);
                    }

                    @Override
                    public void onError() {
                        setNotWished(context, target);
                    }
                });
    }

    /**
     * Update the given {@link ImageView} to display the icon corresponding to the not_favorite state and colorize it.
     *
     * @param context
     * @param target  the target {@link ImageView}
     */
    public static void setNotWished(@NonNull Context context, @NonNull ImageView target) {
        target.clearColorFilter();
        Picasso.with(context)
                .load(R.drawable.ic_action_favorite_outline)
                .into(target);
    }
}
