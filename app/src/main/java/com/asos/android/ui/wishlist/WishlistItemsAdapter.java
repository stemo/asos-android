package com.asos.android.ui.wishlist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asos.android.R;
import com.asos.android.model.CategoryProduct;
import com.asos.android.model.ProcesedProduct;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Stelian Morariu on 12/8/2015.
 */
public class WishlistItemsAdapter extends RecyclerView.Adapter<WishlistItemsAdapter.ViewHolder> {

    private Context mContext;
    private List<ProcesedProduct> mValues;

    public WishlistItemsAdapter(Context context, List<ProcesedProduct> values) {
        this.mContext = context;
        this.mValues = values;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(this.mContext).inflate(R.layout.list_wishlist_product, parent, false);
        return new ViewHolder(v, mContext);

    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        ProcesedProduct current = this.mValues.get(position);
        if (current != null) {
            holder.productName.setText(String.valueOf(current.getProductId()));
            holder.favoritIcon.getDrawable().
                    setColorFilter(mContext.getResources().getColor(R.color.accent),
                            PorterDuff.Mode.SRC_ATOP);
        }

    }

    public int getItemCount() {
        return this.mValues.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.productName)
        TextView productName;

        @Bind(R.id.favoriteIcon)
        ImageView favoritIcon;

        @SuppressLint({"NewApi"})
        public ViewHolder(View itemView, Context context) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
