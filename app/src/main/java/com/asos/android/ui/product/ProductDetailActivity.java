package com.asos.android.ui.product;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.asos.android.Constants;
import com.asos.android.R;
import com.asos.android.model.ProductDetails;
import com.asos.android.rest.AsosRestService;
import com.asos.android.rest.requests.GetCategoriesRequest;
import com.asos.android.rest.requests.GetProductDetailsRequest;
import com.asos.android.ui.shoppingcart.ShoppingCartActivity;
import com.asos.android.ui.shoppingcart.ShoppingCartManager;
import com.asos.android.ui.wishlist.WishListManager;
import com.asos.android.ui.wishlist.WishlistUtils;
import com.asos.android.utils.CategoryType;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.lang.ref.WeakReference;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductDetailActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;

    @Bind(R.id.slider)
    SliderLayout mImageSlide;

    @Bind(R.id.productTitle)
    TextView mProductTitle;

    @Bind(R.id.productPrice)
    TextView mProductPrice;

    @Bind(R.id.favoriteIcon)
    ImageView mFavoriteIcon;

    @Bind(R.id.productDescription)
    TextView mProductDescription;

    @Bind(R.id.productCareInstructions)
    TextView mProductCare;

    @Bind(R.id.productAditionalInfo)
    TextView mProductAditionalInfo;

    /**
     * {@link SpiceManager} responsible with making network requests.
     */
    private SpiceManager mSpiceManager;

    private long mProductId;
    private ProductDetails mProductDetails;
    private WishListManager mWishlistManager;
    private ShoppingCartManager mShopingCartManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);

        // product ID must be passed in the Intent
        mProductId = getIntent().getLongExtra(Constants.PRODUCT_ID, -1l);

        mSpiceManager = new SpiceManager(AsosRestService.class);
        mWishlistManager = WishListManager.getInstance(this);
        mShopingCartManager = ShoppingCartManager.getInstance(this);

        setupToolbar();
        setupImageSlider();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSpiceManager.start(this);

        // get product details from network
        mSpiceManager.execute(new GetProductDetailsRequest(mProductId),
                GetCategoriesRequest.getCacheKey(CategoryType.WOMEN.toString()),
                DurationInMillis.ONE_HOUR, new GetProductDetailsRequestListener(new WeakReference<ProductDetailActivity>(this)));
    }

    @Override
    protected void onStop() {
        mSpiceManager.shouldStop();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Add or remove items from the wishlist when the user clicks the heart-shaped icon.
     */
    @OnClick(R.id.favoriteIcon)
    public void toggleWishlist(View v) {
        v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
        final long productId = mProductDetails.getProductId();
        if (this.mWishlistManager.isProcessed(productId)) {
            this.mWishlistManager.remove(productId);
            WishlistUtils.setNotWished(this, mFavoriteIcon);
        } else {
            this.mWishlistManager.add(productId);
            WishlistUtils.setAsWished(this, mFavoriteIcon);
        }
    }

    @OnClick(R.id.addToCartBtn)
    void addToCart(View v) {
        v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
        final long productId = mProductDetails.getProductId();
        if (this.mShopingCartManager.isProcessed(productId)) {
            showAlreadyInCartDialog();
        } else {
            // check if product is in stock
            if (mProductDetails.isInStock()) {
                showAddToCartDialog();
            } else {
                showOutOfStockDialog();
            }
        }
    }

    /**
     * Notify the user that the current product is out of stock.
     */
    private void showOutOfStockDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dlg_title_error)
                .setMessage(R.string.msg_out_of_stock)
                .setPositiveButton(R.string.action_ok, null) // no listener because dialog will be dismissed automatically
                .show();
    }

    /**
     * Add the product to the shopping cart after the user chooses the desired quantity.
     */
    private void showAddToCartDialog() {
        final View dlgView = LayoutInflater.from(this).inflate(R.layout.dlg_add_to_cart, null);
        final TextView label = (TextView) dlgView.findViewById(R.id.label);
        final Spinner qtSpinner = (Spinner) dlgView.findViewById(R.id.qtSpinner);
        qtSpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.quantities)));

        final Button okBtn = (Button) dlgView.findViewById(R.id.okBtn);
        final Button cancelBtn = (Button) dlgView.findViewById(R.id.cancelBtn);

        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.dlg_shopping_cart)
                .setView(dlgView)
                .setCancelable(false)
                .create();

        // handle ok clicked
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check quantity
                if (qtSpinner.getSelectedItem().toString().equalsIgnoreCase("0")) {
                    label.setError(getString(R.string.err_select_qt));

                    // add listener to remove error message when user selects quantity
                    qtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            label.setError(null);
                            qtSpinner.setOnItemSelectedListener(null);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                } else {
                    // add the product to ths shopping cart
                    mShopingCartManager.add(mProductDetails.getProductId(),
                            Integer.valueOf(qtSpinner.getSelectedItem().toString()));
                    dialog.dismiss();

                    // notify user
                    Snackbar.make(getWindow().getDecorView(), R.string.msg_added_to_shopping_cart, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        // handle cancel clicked
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // show the dialog
        dialog.show();
    }

    /**
     * Show a dialog to notify the user that the item is already in the shopping cart.
     * <p/>
     * The user will have the possibility to view the shopping cart or remove the item .
     */
    private void showAlreadyInCartDialog() {
        // get the current quantity in case the user wants to Undo the operation
        final int quantity = mShopingCartManager.getProcessedProductQuantity(mProductDetails.getProductId());

        new AlertDialog.Builder(this)
                .setTitle(R.string.dlg_shopping_cart)
                .setMessage(R.string.msg_already_in_shopping_cart)
                .setPositiveButton(R.string.action_ok, null)
                .setNegativeButton(R.string.action_remove, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  remove from shopping cart
                        mShopingCartManager.remove(mProductDetails.getProductId());

                        // notify user with the possibility to revert action
                        Snackbar.make(getWindow().getDecorView(),
                                R.string.msg_removed_from_shopping_cart,
                                Snackbar.LENGTH_SHORT)
                                .setAction(R.string.action_undo, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mShopingCartManager.add(mProductDetails.getProductId(),quantity);
                                    }
                                })
                                .show();
                    }
                })
                .setNeutralButton(R.string.action_view_cart, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  open shopping cart
                        startActivity(new Intent(ProductDetailActivity.this, ShoppingCartActivity.class));
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        // hide toolbar title when layout is expanded
        mCollapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
    }

    private void setupImageSlider() {
        mImageSlide.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mImageSlide.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mImageSlide.setCustomAnimation(new DescriptionAnimation());
        mImageSlide.stopAutoCycle();
    }

    /**
     * Display product details
     *
     * @param productDetails
     */
    void updateProductDetails(ProductDetails productDetails) {
        mProductDetails = productDetails;
        mCollapsingToolbarLayout.setTitle(mProductDetails.getBrand());
        mProductTitle.setText(mProductDetails.getTitle());
        mProductPrice.setText(mProductDetails.getCurrentPrice());
        mProductDescription.setText(mProductDetails.getDescription());
        mProductCare.setText(mProductDetails.getCareInfo());
        mProductAditionalInfo.setText(mProductDetails.getAditionalInfo());

        // check if item is in wishlist
        checkWishlist();

        // check if item is already in the shopping cart
        checkShoppingCart();

        loadProductImages();
    }

    /**
     * Check if item is in wishlist and update GUI.
     */
    private void checkWishlist() {
        if (mWishlistManager.isProcessed(mProductDetails.getProductId())) {
            WishlistUtils.setAsWished(this, mFavoriteIcon);
        } else {
            WishlistUtils.setNotWished(this, mFavoriteIcon);
        }
    }

    /**
     * Check if the items is in the shopping cart
     */
    private void checkShoppingCart() {
        if (mShopingCartManager.isProcessed(mProductDetails.getProductId())) {

        }
    }

    /**
     * Add product images to the ImageSlider.
     */
    private void loadProductImages() {
        for (String img : mProductDetails.getProductImages()) {
            BaseSliderView defaultSliderView = new DefaultSliderView(this)
                    .image(img)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop);

            //add your extra information
            defaultSliderView.bundle(new Bundle());
            defaultSliderView.getBundle()
                    .putString("extra", img);

            mImageSlide.addSlider(defaultSliderView);
        }
    }

    private class GetProductDetailsRequestListener implements RequestListener<ProductDetails> {

        final WeakReference<ProductDetailActivity> mActivityReference;

        public GetProductDetailsRequestListener(WeakReference<ProductDetailActivity> activityWeakReference) {
            mActivityReference = activityWeakReference;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            final ProductDetailActivity activity = mActivityReference.get();
            if (activity != null) {
                Snackbar.make(activity.getWindow().getDecorView(), activity.getString(R.string.err_download_product_details),
                        Snackbar.LENGTH_LONG).show();

                // finish the activity after the error message is displayed
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        activity.finish();
                    }
                }, 2000);
            }
        }

        @Override
        public void onRequestSuccess(ProductDetails productDetails) {
            final ProductDetailActivity activity = mActivityReference.get();
            if (activity != null) {
                activity.updateProductDetails(productDetails);
            }
        }
    }
}
