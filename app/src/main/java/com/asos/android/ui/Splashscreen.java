package com.asos.android.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.asos.android.AsosApp;
import com.asos.android.R;
import com.asos.android.model.CategoryListing;
import com.asos.android.rest.AsosRestService;
import com.asos.android.rest.requests.GetCategoriesRequest;
import com.asos.android.ui.browser.AsosBrowserActivity;
import com.asos.android.utils.CategoryListingCallback;
import com.asos.android.utils.CategoryType;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.lang.ref.WeakReference;

/**
 * ASOS splash screen activity is responsible with downloading all the data required
 * for the application to start.
 * While download is in progress, user will be shown a blank screen with a logo.
 */
public class Splashscreen extends AppCompatActivity implements CategoryListingCallback {
    private static final String LOG_TAG = Splashscreen.class.getSimpleName();
    /**
     * {@link SpiceManager} responsible with making network requests.
     */
    private SpiceManager mSpiceManager;

    private AsosApp mApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        mSpiceManager = new SpiceManager(AsosRestService.class);
        mApp = (AsosApp) getApplicationContext();
    }


    @Override
    protected void onStart() {
        mSpiceManager.start(this);
        super.onStart();
        checkPrerequisites();
    }

    @Override
    protected void onStop() {
        mSpiceManager.shouldStop();
        super.onStop();
    }

    @Override
    public void onCategoryListingDownloaded(@NonNull CategoryListing categoryListing,
                                            @NonNull CategoryType categoryType) {
        // set the new downloaded category lsitings
        if (categoryType.equals(CategoryType.WOMEN)) {
            mApp.setWomensCategory(categoryListing);
        } else {
            mApp.setMensCategory(categoryListing);
        }

        // check again if all conditions are met
        checkPrerequisites();
    }

    @Override
    public void onError(String error) {
        Log.e(LOG_TAG, error);
        final AlertDialog dlg = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.dlg_title_error))
                .setMessage(getString(R.string.err_download_categories))
                .setPositiveButton(getString(R.string.action_ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();
    }

    /**
     * Check if the app has all the data required to start.
     */
    private void checkPrerequisites() {
        boolean hasWomenCategories = false;
        boolean hasMenCategories = false;

        // check women categories
        if (mApp.getWomensCategory() == null) {
            // by providing a cache key and a cache expiration value, Robospice will cache values returned  for this request
            // if values exist in cache and are not expired they are returned immediately, otherwise the request executes normally
            mSpiceManager.execute(new GetCategoriesRequest(CategoryType.WOMEN),
                    GetCategoriesRequest.getCacheKey(CategoryType.WOMEN.toString()),
                    DurationInMillis.ONE_HOUR,
                    new GetCategoriesRequestListener(new WeakReference<CategoryListingCallback>(this),
                            CategoryType.WOMEN));
        } else {
            hasWomenCategories = true;
        }

        // check men categories
        if (mApp.getMensCategory() == null) {
            // by providing a cache key and a cache expiration value, Robospice will cache values returned  for this request
            // if values exist in cache and are not expired they are returned immediately, otherwise the request executes normally
            mSpiceManager.execute(new GetCategoriesRequest(CategoryType.MEN),
                    GetCategoriesRequest.getCacheKey(CategoryType.MEN.toString()),
                    DurationInMillis.ONE_HOUR,
                    new GetCategoriesRequestListener(new WeakReference<CategoryListingCallback>(this),
                            CategoryType.MEN));
        } else {
            hasMenCategories = true;
        }

        // start main activity if we have both categories
        if (hasWomenCategories && hasMenCategories) {
            startActivity(new Intent(this, AsosBrowserActivity.class));
            finish();
        }
    }

    /**
     * Request listener for downloading {@link CategoryListing} objects.
     * <p/>
     * The activity/fragment will be notified of request success or failure through a {@link CategoryListingCallback} object.
     */
    private class GetCategoriesRequestListener implements RequestListener<CategoryListing> {

        private CategoryListingCallback mCallback;
        private CategoryType mCategoryType;

        public GetCategoriesRequestListener(final WeakReference<CategoryListingCallback> callbackWeakReference,
                                            final CategoryType categoryType) {
            mCallback = callbackWeakReference.get();
            mCategoryType = categoryType;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            if (mCallback != null) {
                mCallback.onError(spiceException.getMessage());
            }
        }

        @Override
        public void onRequestSuccess(CategoryListing category) {
            Log.d(LOG_TAG, category.getDescription());
            if (mCallback != null) {
                mCallback.onCategoryListingDownloaded(category, mCategoryType);

            }
        }
    }
}
