package com.asos.android.ui.browser;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.asos.android.AsosApp;
import com.asos.android.R;
import com.asos.android.model.Category;
import com.asos.android.model.CategoryProductListWrapper;
import com.asos.android.model.CategoryProduct;
import com.asos.android.rest.AsosRestService;
import com.asos.android.rest.requests.GetCategoryProductsRequest;
import com.asos.android.ui.shoppingcart.ShoppingCartActivity;
import com.asos.android.ui.wishlist.WishlistActivity;
import com.asos.android.utils.CategoryType;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AsosBrowserActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String LOG_TAG = AsosBrowserActivity.class.getSimpleName();

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @Bind(R.id.navigation_view)
    NavigationView mNavigationView;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.categorySpinner)
    Spinner mCategorySpinner;

    @Bind(R.id.subCategorySpinner)
    Spinner mSubCategorySpinner;

    @Bind(R.id.items)
    RecyclerView mItemsRecyclerView;

    /**
     * {@link SpiceManager} responsible with making network requests.
     */
    private SpiceManager mSpiceManager;
    private AsosApp mApp;
    private SubCategoryAdapter mSubCategoryAdapter;
    private ItemsAdapter mItemsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        ButterKnife.bind(this);

        mApp = (AsosApp) getApplicationContext();
        mSpiceManager = new SpiceManager(AsosRestService.class);

        // setup toolbar
        setupToolbar();

        // setup navigation
        mNavigationView.setNavigationItemSelectedListener(this);

        setupFilters();
        setupItemsList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSpiceManager.start(this);

        // add listener after we created the mSubCategoryAdapter
        mCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateSubCategories();

                // update items only if the adapter has been initialized
                if (mItemsAdapter != null) {
                    updateItems();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // this state is not used
            }
        });

    }

    @Override
    protected void onStop() {
        mSpiceManager.shouldStop();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        // we only set menu item checked if the menu entry will start a navigation activity
       // menuItem.setChecked(true);

        switch (menuItem.getItemId()) {
            case R.id.drawer_cart:
                startActivity(new Intent(this, ShoppingCartActivity.class));
                break;
            case R.id.drawer_wishlist:
                startActivity(new Intent(this, WishlistActivity.class));
                break;
        }

        mDrawerLayout.closeDrawers();
        return true;
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Update the displayed items list based on the selected subcategory
     */
    private void updateItems() {
        Category selected = (Category) mSubCategorySpinner.getSelectedItem();

        if (selected != null) {
            mSpiceManager.execute(new GetCategoryProductsRequest(selected.getCategoryId()),
                    GetCategoryProductsRequest.getCacheKey(String.valueOf(selected.getCategoryId())),
                    DurationInMillis.ONE_HOUR,
                    new GetCategoryProductsRequestListener(new WeakReference<ItemsAdapter>(mItemsAdapter),
                            new WeakReference<AsosBrowserActivity>(this)));
        }
    }

    /**
     * update the subcategory entries based on selected category type
     */
    private void updateSubCategories() {
        try {
            final CategoryType selection = CategoryType.valueOf(mCategorySpinner.getSelectedItem().toString());
            mSubCategoryAdapter.clear();
            switch (selection) {
                case WOMEN:
                    mSubCategoryAdapter.addAll(new ArrayList<>(mApp.getWomensCategory().getListing()));
                    break;
                case MEN:
                    mSubCategoryAdapter.addAll(new ArrayList<>(mApp.getMensCategory().getListing()));
                    break;
            }

            mCategorySpinner.refreshDrawableState();
            mSubCategorySpinner.refreshDrawableState();
        } catch (IllegalArgumentException e) {
            Log.e(LOG_TAG, e.toString());
            Snackbar.make(mToolbar, getString(R.string.err_category_cast), Snackbar.LENGTH_SHORT).show();
        }
    }

    private void setupFilters() {
        //setup category filter
        mCategorySpinner.setAdapter(new CategoryAdapter(this));

        // initiate sub-category filter based on category selection
        mSubCategoryAdapter = new SubCategoryAdapter(this, new ArrayList<>(mApp.getWomensCategory().getListing()));
        mSubCategorySpinner.setAdapter(mSubCategoryAdapter);

        // select Womens category by default
        mCategorySpinner.setSelection(0);
    }

    private void setupItemsList() {
        mItemsRecyclerView.setHasFixedSize(true);
        mItemsRecyclerView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
        mItemsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // create empty adapter
        mItemsAdapter = new ItemsAdapter(this, new ArrayList<CategoryProduct>());
        mItemsRecyclerView.setAdapter(mItemsAdapter);
    }

    private class GetCategoryProductsRequestListener implements RequestListener<CategoryProductListWrapper> {

        private WeakReference<ItemsAdapter> mItemsAdapterWeakReference;
        private WeakReference<AsosBrowserActivity> mActivityWeakReference;

        /**
         * Create a new {@link com.asos.android.ui.browser.AsosBrowserActivity.GetCategoryProductsRequestListener}
         * listener using {@link WeakReference} objects
         *
         * @param itemsAdapterWeakReference
         * @param activityWeakReference
         */
        public GetCategoryProductsRequestListener(WeakReference<ItemsAdapter> itemsAdapterWeakReference,
                                                  WeakReference<AsosBrowserActivity> activityWeakReference) {

            mItemsAdapterWeakReference = itemsAdapterWeakReference;
            mActivityWeakReference = activityWeakReference;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            final AsosBrowserActivity asosBrowserActivity = mActivityWeakReference.get();
            if (asosBrowserActivity != null) {
                Snackbar.make(asosBrowserActivity.getWindow().getDecorView(),
                        asosBrowserActivity.getString(R.string.err_download_products),
                        Snackbar.LENGTH_SHORT)
                        .show();
            }
        }

        @Override
        public void onRequestSuccess(CategoryProductListWrapper categoryProducts) {
            final ItemsAdapter itemsAdapter = mItemsAdapterWeakReference.get();
            if (itemsAdapter != null) {
                itemsAdapter.updateItems(categoryProducts.getItems());
            }
        }
    }
}
