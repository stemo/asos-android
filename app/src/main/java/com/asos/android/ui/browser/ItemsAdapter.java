package com.asos.android.ui.browser;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asos.android.Constants;
import com.asos.android.R;
import com.asos.android.model.CategoryProduct;
import com.asos.android.ui.product.ProductDetailActivity;
import com.asos.android.ui.wishlist.WishListManager;
import com.asos.android.ui.wishlist.WishlistUtils;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Stelian Morariu on 12/8/2015.
 */
public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoryProduct> mValues;
    private WishListManager mWishlistManager;
    private View.OnClickListener mOnFavoriteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    public ItemsAdapter(Context context, List<CategoryProduct> values) {
        this.mContext = context;
        this.mValues = values;

        mWishlistManager = WishListManager.getInstance(mContext);
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(this.mContext).inflate(R.layout.list_item_product, parent, false);
        return new ViewHolder(v, mWishlistManager, mContext);

    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        CategoryProduct current = this.mValues.get(position);
        if (current != null) {
            holder.productName.setText(current.getTitle());
            holder.productPrice.setText(current.getCurrentPrice());
            holder.categoryProduct = current;
            Picasso.with(mContext).load(current.getProductImages().get(0))
                    .fit()
                    .centerInside()
                    .into(holder.productImage);

            // update product wishlist status
            holder.updateWishlistStatus();
        }

    }

    public int getItemCount() {
        return this.mValues.size();
    }

    public void updateItems(List<CategoryProduct> items) {
        this.mValues.clear();
        this.mValues.addAll(items);
        this.notifyDataSetChanged();
    }

    public List<CategoryProduct> getItems() {
        return Collections.unmodifiableList(this.mValues);
    }

    public CategoryProduct getItemAtPosition(int position) {
        return this.mValues.get(position);
    }

    class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {

        @Bind(R.id.productImage)
        ImageView productImage;

        @Bind(R.id.favoriteIcon)
        ImageView favoriteIcon;

        @Bind(R.id.productName)
        TextView productName;

        @Bind(R.id.productPrice)
        TextView productPrice;

        CategoryProduct categoryProduct;

        private WishListManager mWishlistManager;
        private Context mContext;

        @SuppressLint({"NewApi"})
        public ViewHolder(View itemView, WishListManager wishlistManager, Context context) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            this.mWishlistManager = wishlistManager;
            this.mContext = context;

            // use a gesture detector to handle single and double taps
            final GestureDetector gestureDetector = new GestureDetector(context, new GestureListener());
            productImage.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gestureDetector.onTouchEvent(event);
                }
            });
        }


        /**
         * Add or remove items from the wishlist
         */
        @OnClick(R.id.favoriteIcon)
        public void toggleWishlist(View v) {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            if (this.mWishlistManager.isProcessed(categoryProduct.getProductId())) {
                this.mWishlistManager.remove(categoryProduct.getProductId());
                WishlistUtils.setNotWished(mContext, favoriteIcon);
            } else {
                this.mWishlistManager.add(categoryProduct.getProductId());
                WishlistUtils.setAsWished(mContext, favoriteIcon);
            }
        }

        /**
         * Launch product detail activity.
         *
         * @param v
         */
        public void launchProductDetail(View v) {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            final Intent intent = new Intent(this.mContext, ProductDetailActivity.class);
            intent.putExtra(Constants.PRODUCT_ID, categoryProduct.getProductId());
            this.mContext.startActivity(intent);
        }


        /**
         * Check if the current product is in the wishlist and update the favorite icon accordingly.
         */
        public void updateWishlistStatus() {
            if (this.mWishlistManager.isProcessed(categoryProduct.getProductId())) {
                WishlistUtils.setAsWished(mContext, favoriteIcon);
            } else {
                WishlistUtils.setNotWished(mContext, favoriteIcon);
            }
        }


        // Class that helps us intercept double clicks
        private class GestureListener extends GestureDetector.SimpleOnGestureListener {

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                launchProductDetail(productImage);
                return true;
            }

            // event when double tap occurs
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                toggleWishlist(productImage);
                return true;
            }
        }
    }

}
