package com.asos.android.ui.shoppingcart;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.asos.android.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ShoppingCartActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.items)
    RecyclerView mItemsRecyclerView;

    private ShoppingCartManager mShopingCartManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        ButterKnife.bind(this);

        mShopingCartManager = ShoppingCartManager.getInstance(this);

        setupToolbar();
        setupItemsList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void setupItemsList() {
        mItemsRecyclerView.setHasFixedSize(true);
        mItemsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mItemsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // create empty adapter
        final ShoppingCartItemsAdapter adapter = new ShoppingCartItemsAdapter(this, mShopingCartManager.getItems());
        mItemsRecyclerView.setAdapter(adapter);
    }
}
