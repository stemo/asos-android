package com.asos.android.ui.browser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.asos.android.R;
import com.asos.android.model.Category;

import java.util.List;

/**
 * Created by Stelian Morariu on 12/8/2015.
 */
public class SubCategoryAdapter extends ArrayAdapter<Category> {

    private Context mContext;

    public SubCategoryAdapter(Context context, List<Category> objects) {
        super(context, R.layout.spinner_item_category, objects);
        mContext = context;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, false);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent, boolean isDropDown) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.spinner_item_category, parent, false);
        }
        final Category category = getItem(position);
        final TextView categoryName = (TextView) convertView.findViewById(R.id.name);
        categoryName.setText(category.getName());

        return convertView;
    }

}
