package com.asos.android.ui.shoppingcart;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.asos.android.R;
import com.asos.android.model.CategoryProduct;
import com.asos.android.model.ProcesedProduct;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Stelian Morariu on 12/8/2015.
 */
public class ShoppingCartItemsAdapter extends RecyclerView.Adapter<ShoppingCartItemsAdapter.ViewHolder> {

    private Context mContext;
    private List<ProcesedProduct> mValues;

    public ShoppingCartItemsAdapter(Context context, List<ProcesedProduct> values) {
        this.mContext = context;
        this.mValues = values;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(this.mContext).inflate(R.layout.list_processed_item_product, parent, false);
        return new ViewHolder(v, mContext);

    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        ProcesedProduct current = this.mValues.get(position);
        if (current != null) {
            holder.productName.setText(String.valueOf(current.getProductId()));
            holder.productQuantity.setText(String.valueOf(current.getQuantity()));
        }

    }

    public int getItemCount() {
        return this.mValues.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.productName)
        TextView productName;

        @Bind(R.id.productQuantity)
        TextView productQuantity;

        @SuppressLint({"NewApi"})
        public ViewHolder(View itemView, Context context) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
