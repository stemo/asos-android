package com.asos.android.ui.wishlist;

import android.content.Context;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.asos.android.Constants;
import com.asos.android.model.ProcesedProduct;
import com.asos.android.utils.ProcessedItemsManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class designed to handle wishlist items.
 * <p/>
 * Created by Stelian Morariu on 13/8/2015.
 */
public class WishListManager extends ProcessedItemsManager {

    private static WishListManager mInstance;
    private static List<ProcesedProduct> mProcessedProducts;


    private WishListManager() {
        // make constructors private so we can use WishListManager as singleton
    }

    private WishListManager(Context context) {
        mContext = context;
        mGson = new Gson();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mProcessedProductListType = new TypeToken<List<ProcesedProduct>>() {
        }.getType();
    }

    /**
     * Get an instance of {@link WishListManager}.
     *
     * @param context
     */
    public static WishListManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new WishListManager(context);
            mProcessedProducts = new ArrayList<>();

            // load items from prefs
            final String json = mSharedPreferences.getString(Constants.WISHLIST_KEY, "");
            if (!TextUtils.isEmpty(json)) {
                final ArrayList<ProcesedProduct> items = mGson.fromJson(json, mProcessedProductListType);
                mProcessedProducts.addAll(items);
            }

        }

        return mInstance;
    }


    @Override
    protected String getPreferenceKey() {
        return Constants.WISHLIST_KEY;
    }

    @Override
    protected List<ProcesedProduct> getItemsList() {
        return mProcessedProducts;
    }
}
