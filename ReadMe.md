#ASOS challenge design decisions

The application developed for the ASOS challenge was designed after reading the provided material, analysing the existing ASOS android app and taking in consideration that one of the assignments was to redesign the  GUI to make it a better Android experience.

##1.GUI
The main difference you will see in the app, when compared to the mock-ups and existing versions is the navigation drawer.  Previously, the navigation drawer was home for the product categories (men,women) and subcategories while other options (such as help and settings) where placed as icons in the ActionBar/Toolbar . 

While this strategy cleared up space on the main screen, I don’t think it provides a good user experience : I had to play with the app quite a while until I realised that the option buttons change when you open the drawer. My approach was to move all the “static” content (help,settings, wishlist, etc.) to the navigation drawer and create a filter-like control to browse products. 


I noticed that on the live version of the app there are sort options, to accommodate those controls we could move the category filter up in the toolbar.

Double tapping on a product image will add it/ remove it to/from the wishlist. This can also be achieved by pressing the heart shaped icon.


![](https://bytebucket.org/stemo/asos-android/raw/0e3e85a41bb87e9cd9bb6f8c54b2ab1c8f523d17/images/device-2015-08-14-141400.png)

![](https://bytebucket.org/stemo/asos-android/raw/0e3e85a41bb87e9cd9bb6f8c54b2ab1c8f523d17/images/device-2015-08-14-142040.png)

Moving on to the product detail page, you will notice that I used a collapsable layout that holds the image slider control.

![](https://bytebucket.org/stemo/asos-android/raw/0e3e85a41bb87e9cd9bb6f8c54b2ab1c8f523d17/images/device-2015-08-14-142500.png)

![](https://bytebucket.org/stemo/asos-android/raw/0e3e85a41bb87e9cd9bb6f8c54b2ab1c8f523d17/images/device-2015-08-14-143251.png)


##2. Performance
I understand performance is critical to an application that needs to load so many informations  so I used some 3rd party, open source  libraries to cover that aspect.


###Network requests

For regular network requests, such as getting a list of subcategories, I chose Robospice + Retrofit.  Robospice is a library specialised in network requests that makes writing and managing  async  tasks very easy and it also has support for caching requests - this means that there is very little code that you need to write if you want to save data to cache.  Retrofit is a library that allows us to write an easy to understand/maintain  API over our REST service. The combination of the 2 libraries provides a good solution for ASOS network requests.

###Image loading

For loading images I used another combination of open source libraries, Picasso and OkHttp.
Picasso is a very cool library that knows how to download images, resize, rescale or apply some transformation  and then load them into specific views. The library is very easy to use and if you combine it with OkHttp you get automatic caching of the images .

##3. Final words
Despite not being complete, I believe that the app,along with this document can give you an idea about my thought and work process.

